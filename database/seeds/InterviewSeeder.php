<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'date' => '05.04.20',
                'information' => 'good interview',
                'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
               
            ],
            [
                'date' => '03.05.20',
                'information' => 'I want him in my team!',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
              
            ],                      
            ]);
    }
}
