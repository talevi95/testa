@extends('layouts.app')

@section('title', 'Create candidate')

@section('content')
        <h1>Create interview</h1>
        <form method = "post" action = "{{action('InformationController@store')}}">
        @csrf
        
        <label for = "interviewer">Interviewer name</label>
        <select class="form-control" name="interviewer_id">
        @foreach($interviewer as $interviewer)
        @if(isset($interviewer->id))
        <option value=" {{$interviewer->id}} ">{{$interviewer->name}}</option>
        @else
            Assign owner
        @endif 
        @endforeach 
        </select>
        </br>
        <label for = "Candidate">Candidate Name</label>
        <select class="form-control" name="candidate_id">
        @foreach($candidates as $candidate)
        @if(isset($candidate->id))
        <option value=" {{$candidate->id}} ">{{$candidate->name}}</option>  
        @else
            Assign owner
        @endif
        
        @endforeach
        </select>
        </br>
        <div class="form-group">
            <label for = "date">Date</label>
            <input type="date" name="date" id="date" class="form-control" value="{{ date("Y-m-d") }}" required />
        <div class="form-group">
        </br>
            <label for = "information">Information</label>
            <input type = "text" class="form-control" name = "information">
        </div> 
        <div>
            <input type = "submit" name = "submit" value = "Create interview">
        </div>                       
        </form>    
@endsection