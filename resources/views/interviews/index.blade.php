@extends('layouts.app')


@section('content')

<div><a href="{{route('interviews.create')}}">Create New Interview</a></div>
<h1>List Of Interviews</h1>

<table class = "table">
    <tr>
    <th>Candidate Name</th><th>Interviewer Name</th><th>Date</th><th>Information</th>
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>
            <td></td>
            <td></td>
            <td>{{$interview->date}}</td>
            <td>{{$interview->information}}</td>                                                           
        </tr>
    @endforeach
</table>
@endsection